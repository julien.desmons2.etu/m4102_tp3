package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {


    @Transaction
    default void createTableAndIngredientAssociation() {
    	createTable();
    	createAssociationTable();
    }
    
    @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation (idPizza VARCHAR(128), idIngredient VARCHAR(128))")
    void createAssociationTable();
	
	@SqlUpdate("CREATE TABLE IF NOT EXISTS pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
    void createTable();
	
	default void dropTableAndIngredientAssociation() {
	      dropAssociationTable();
	      dropTable();
	    }

    @SqlUpdate("DROP TABLE IF EXISTS pizzas")
    void dropTable();
    
    @SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
    void dropAssociationTable();
    
    @Transaction
    default void insertPizzaWithIngredients(Pizza pizza) {
    	insertPizza(pizza.getId(),pizza.getName());
    	for(Ingredient ingredient : pizza.getIngredients()) {
    		insertIngredient(pizza.getId(), ingredient.getId());
    	}
    }

    @SqlUpdate("INSERT INTO pizzas (id, name) VALUES (:id, :name)")
    void insertPizza(@Bind("id") UUID id, @Bind("name") String name);
    
    @SqlUpdate("INSERT INTO PizzaIngredientsAssociation (:idPizza, :idIngredient)")
    void insertIngredient(@Bind("idPizza") UUID idPizza, @Bind("idIngredient") UUID idIngredient);
    
    @Transaction
    default void remove(UUID id) {
    	removePizza(id);
    	removeIngredients(id);
    }

    @SqlUpdate("DELETE FROM pizzas WHERE id = :id")
    void removePizza(@Bind("id") UUID id);
    
    @SqlUpdate("DELETE FROM PizzaIngredientsAssociation WHERE idPizza = :id")
    void removeIngredients(@Bind("id") UUID id);
    
    @Transaction
    default Pizza findByName(String name) {
    	Pizza laPizza = findPizzaByName(name);
    	//laPizza.setIngredients(findIngredientsById(laPizza.getId()));
    	return laPizza;
    }

    @SqlQuery("SELECT * FROM pizzas WHERE name = :name")
    @RegisterBeanMapper(Pizza.class)
    Pizza findPizzaByName(@Bind("name") String name);

    @SqlQuery("SELECT * FROM pizzas")
    @RegisterBeanMapper(Pizza.class)
    List<Pizza> getAll();
    
    @Transaction
    default Pizza findById(UUID id) {
    	Pizza laPizza = findPizzaById(id);
    	//laPizza.setIngredients(findIngredientsById(laPizza.getId()));
    	return laPizza;
    }

    @SqlQuery("SELECT * FROM pizzas WHERE id = :id")
    @RegisterBeanMapper(Pizza.class)
    Pizza findPizzaById(@Bind("id") UUID id);
    
    @SqlQuery("SELECT * FROM ingredients WHERE id IN (SELECT idIngredient FROM PizzaIngredientsAssociation WHERE idPizza = :id)")
    @RegisterBeanMapper(Ingredient.class)
    List<Ingredient> findIngredientsById(@Bind("id") UUID id);
}
