| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                              |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------                |
| /pizzas             | GET         | <-application/json<br><-application/xml                      |                 | liste des pizzas (P2)                                           |
| /pizzas/{id}        | GET         | <-application/json<br><-application/xml                      |                 | une pizza (P2) ou 404                                            |
| /pizzas/{id}/name   | GET         | <-text/plain                                                 |                 | le nom de la pizza ou 404                                        |
| /pizzas             | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Pizza (P1) Ingrédient* (I1) | Nouvelle pizza (P2)<br>409 si la pizza existe déjà (même nom)<br>412 si un ou plusieurs des ingrédients d'existent pas |
| /pizzas/{id}        | DELETE      |                                                              |         

JSON P2 :   
     
	{
		"id": "f38806a8-7c85-49ef-980c-149dcd81d306",
		"name": "rosetta",
		"list":[{"id": "f38806a8-7c85-49ef-980c-149dcd81d306","name": "mozzarella"},{"id": "f38806a9-7c85-49ef-980c-149dcd81d302","name": "sauce tomate"}]
	}

JSON P1 :

	{
		"name": "rosetta",
		"list":[{"name": "mozzarella"},{"name": "sauce tomate"}]
	}
